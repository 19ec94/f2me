import sys
import dolfin as df                                                    
import numpy as np                                                      
import os                                                               
import time as time_module 
import matplotlib.pyplot as plt
import csv                                                              
import yaml #version higher than 5.0
import math

df.parameters['ghost_mode']= 'shared_facet' #for parallel program          
#My library      

#Inputs                                  
if len(sys.argv) >1 :
    user_given_input_file= sys.argv[1]
else:
    print("Provide an input file")
    quit()

with open(user_given_input_file,'r') as input_file:
    input_file = yaml.load(input_file, Loader=yaml.FullLoader)

# reference parameter
ref_rho = input_file["GENERAL_PARAMS"]['ref_rho']
ref_theta = input_file["GENERAL_PARAMS"]['ref_theta']
ref_vx = input_file["GENERAL_PARAMS"]['ref_vx']
ref_vy = input_file["GENERAL_PARAMS"]['ref_vy']

for current_mesh in range(len(input_file["MESH_PARAMS"]["mesh_list"])):
    #MESH
    #input
    my_current_mesh = input_file["MESH_PARAMS"]['mesh_list'][current_mesh] 
    mesh_number = int(list(my_current_mesh)[-4])  
    #mesh function
    mesh = df.Mesh()
    hdf =  df.HDF5File(mesh.mpi_comm(),my_current_mesh,"r")
    hdf.read(mesh,"/mesh", False)
    dim = mesh.topology().dim()
    subdomains = df.MeshFunction("size_t", mesh, dim)
    hdf.read(subdomains, "/subdomains")
    boundaries = df.MeshFunction("size_t", mesh, dim-1)
    hdf.read(boundaries, "/boundaries")
    nv = df.FacetNormal(mesh) #normal vector
    ds = df.Measure("ds",domain=mesh, subdomain_data=boundaries)               
    dS = df.Measure("dS",domain=mesh, subdomain_data=boundaries) 
    

    #FUNCTION SPACE
    #input
    number_of_moments = input_file["GENERAL_PARAMS"]['number_of_moments']
    problem_type = input_file["GENERAL_PARAMS"]['problem_type']
    # function space function
    V = df.VectorFunctionSpace(mesh,'P',1,dim=number_of_moments)
    #Set test function(s)
    v_list = df.TestFunctions(V) 
    #Convert to ufl form
    v = df.as_vector(v_list) 
    #set trial function(s)
    if problem_type == 'nonlinear':
        u_list = df.Function(V) 
    elif problem_type == 'linear':
        u_list = df.TrialFunctions(V)
    else:
        print("Undefined problem_type paramter is passed")
        quit()
    #Convert to ufl form
    u = df.as_vector(u_list) 


    #SYSTEM MATRIX
    #input
    moment_order=input_file["GENERAL_PARAMS"]['moment_order']
    Kn = input_file["GENERAL_PARAMS"]['Kn']
    chi = input_file["BOUNDARY_PARAMS"]['chi']
    epsilon_w = input_file["BOUNDARY_PARAMS"]['epsilon_w']
    chi2 = math.sqrt(2/math.pi)
    A_x = np.array([
            [0, 1, 0, 0, 0, 0, 0, 0, 0], 
            [1, 0, 0, -0.6666666666666666, 1, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 1, 0, 0, 0], 
            [0, -1, 0, 0, 0, 0, 0, 1, 0], 
            [0, 1.3333333333333333, 0, 0, 0, 0, 0, -0.5333333333333333, 0], 
            [0, 0, 1, 0, 0, 0, 0, 0, -0.4], 
            [0, -0.6666666666666666, 0, 0, 0, 0, 0, 0.26666666666666666, 0], 
            [0, 0, 0, 1.6666666666666667, -1, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, -1, 0, 0, 0]
            ])
    A_y = np.array([
            [0, 0, 1, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 1, 0, 0, 0], 
            [1, 0, 0, -0.6666666666666666, 0, 0, 1, 0, 0], 
            [0, 0, -1, 0, 0, 0, 0, 0, 1], 
            [0, 0, -0.6666666666666666, 0, 0, 0, 0, 0, 0.26666666666666666], 
            [0, 1, 0, 0, 0, 0, 0, -0.4, 0], 
            [0, 0, 1.3333333333333333, 0, 0, 0, 0, 0, -0.5333333333333333], 
            [0, 0, 0, 0, 0, -1, 0, 0, 0], 
            [0, 0, 0, 1.6666666666666667, 0, 0, -1, 0, 0]
            ])
    Block_A = np.array([
            [1, 0, -0.6666666666666666, 1, 0, 0], 
            [0, 1, 0, 0, 0, -0.4], 
            [0, 0, 0.6666666666666666, -0.4, 0, 0]
            ])
    L_matrix = np.array([
            [epsilon_w, 0.0, 0.0],
            [0.0, chi2, 0.0],
            [0.0, 0.0, 2*chi2]
            ])
    P_Coeff = np.array([
            [0, 0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 1/Kn, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 1/Kn, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 1/Kn, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 2/(3*Kn), 0], 
            [0, 0, 0, 0, 0, 0, 0, 0, 2/(3*Kn)]
            ])
    P_even = np.array([
            [1, 0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 1, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 1, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 1, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 1, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0, 1]
            ])
    P_odd = np.array([
            [0, 1, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 1, 0, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 1, 0]
            ])
    Symm = np.array([
            [1, 0, 0, 0, 0, 0, 0, 0, 0], 
            [0, 1, 0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 1, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0.6666666666666666, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 1, 0, 0.5, 0, 0], 
            [0, 0, 0, 0, 0, 1, 0, 0, 0], 
            [0, 0, 0, 0, 0.5, 0, 1, 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0.4, 0], 
            [0, 0, 0, 0, 0, 0, 0, 0, 0.4]
            ])
    T_n = np.array([
            [1, 0, 0, 0, 0, 0, 0, 0, 0], 
            [0, nv[0], nv[1], 0, 0, 0, 0, 0, 0], 
            [0, -nv[1], nv[0], 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 1, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, pow(nv[0],2), 2*nv[0]*nv[1], pow(nv[1],2), 0, 0], 
            [0, 0, 0, 0, -nv[0]*nv[1], pow(nv[0],2) - pow(nv[1],2), nv[0]*nv[1], 0, 0], 
            [0, 0, 0, 0, pow(nv[1],2), -2*nv[0]*nv[1], pow(nv[0],2), 0, 0], 
            [0, 0, 0, 0, 0, 0, 0, nv[0], nv[1]], 
            [0, 0, 0, 0, 0, 0, 0, -nv[1], nv[0]]
            ])

    #system matrix function
    Block_A_trans = np.transpose(Block_A)
    L_inverse = np.linalg.inv(L_matrix)
    SA_x = np.dot(Symm,A_x)
    SA_y = np.dot(Symm,A_y)
    SP_Coeff = np.dot(Symm,P_Coeff)
    #Calculate matrices of boundary integral term
    BC1 = np.dot(np.dot(np.dot(np.transpose(T_n),\
        np.transpose(P_odd)),L_inverse),np.dot(P_odd,T_n))
    BC2 = np.dot(np.dot(np.dot(np.dot(np.dot(np.transpose(T_n),\
        np.transpose(P_even)),Block_A_trans),L_matrix),\
        Block_A),np.dot(P_even,T_n))
    #boundary integral form form inhomogeneity
    BC1_rhs = np.dot(np.transpose(T_n),\
            np.dot(np.transpose(P_even),Block_A_trans))
    BC2_rhs = np.dot(np.transpose(T_n),\
            np.dot(np.transpose(P_odd),L_inverse))
    # UFL conversion
    SA_x= df.as_matrix(SA_x)
    SA_y= df.as_matrix(SA_y)
    SP_Coeff= df.as_matrix(SP_Coeff)
    BC1= df.as_matrix(BC1)
    BC2= df.as_matrix(BC2)
    BC1_rhs= df.as_matrix(BC1_rhs)
    BC2_rhs= df.as_matrix(BC2_rhs)
    

    #VARIATIONAL PROBLEM
    a =0.0
    df.ds = ds #Import <----
    a += (+0.5 * ((df.inner(v, (SA_x * df.Dx(u, 0)))) +\
            (df.inner(v, (SA_y * df.Dx(u, 1))))) ) * df.dx 
    a += (-0.5 * ((df.inner(df.Dx(v, 0),(SA_x * u))) +\
            (df.inner(df.Dx(v, 1),(SA_y * u)))) ) * df.dx 
    a += (+0.5 * df.inner(v, ((BC1 + BC2) * u)) ) * df.ds 
    a += (df.inner(v, (SP_Coeff * u)) ) * df.dx 

    #Stabilization
    df.dS = dS #Important <----
    h_msh = df.CellDiameter(mesh)                                              
    h_avg = ( h_msh("+") + h_msh("-") )/2.0                                 
    gls = (0.0001* h_msh * df.inner(SA_x * df.Dx(u,0)\
            + SA_y * df.Dx(u,1) + SP_Coeff * u, SA_x * df.Dx(v,0)\
            + SA_y * df.Dx(v,1) + SP_Coeff * v ) * df.dx)

    #Inhomogeneity
    def inhomogeneity(bc_id):
        phi_local = df.Expression("atan2(x[1],x[0])",degree=2)
        chi2 = math.sqrt(2/math.pi)
        epsilon_w = input_file["BOUNDARY_PARAMS"]['epsilon_w']
        chi = input_file["BOUNDARY_PARAMS"]['chi']

        theta_w= input_file["BOUNDARY_PARAMS"]['bc'][bc_id]['theta_w']
        u_t_w= input_file["BOUNDARY_PARAMS"]['bc'][bc_id]['u_t_w']
        u_n_w= input_file["BOUNDARY_PARAMS"]['bc'][bc_id]['u_n_w']
        p_w= input_file["BOUNDARY_PARAMS"]['bc'][bc_id]['p_w']
        #Input
        u_n_w= df.Expression("{}".format(u_n_w),degree=2,phi=phi_local)
        u_t_w= df.Expression("{}".format(u_t_w),degree=2,phi=phi_local)
        p_w= df.Expression("{}".format(p_w),degree=2,phi=phi_local)
        theta_w= df.Expression("{}".format(theta_w),degree=2,phi=phi_local)
        #*******
        G_rhs_list = [
                -epsilon_w * (p_w/ref_rho),
                -math.sqrt(2/math.pi) * (u_t_w/math.sqrt(ref_theta)), 
                2*math.sqrt(2/math.pi)* (theta_w/ref_theta) 
                ]
        return df.as_vector(G_rhs_list) #UFL form conversion

    # inhomogeneity boundary conditions
    bc = 0.0 #initialise
    df.ds = ds #<---- Important
    bc += sum([(-0.5 * df.inner(v,((BC1_rhs-BC2_rhs) \
            * inhomogeneity(bc_id)))) * df.ds(bc_id) for bc_id in
            input_file["BOUNDARY_PARAMS"]["bc"].keys()])#RHS-2
    def nonlinearity():
        nl =0.0 
        nl = np.array(
                [
                    df.Constant(0.0),
                    df.Constant(0.0),
                    df.Constant(0.0),
                    df.Constant(0.0),
                    (u[0]*u[4] - (2.0/3.0) * u[1]**2 + (1.0/3.0) * u[2]**2), 
                    (u[0]*u[5] - u[1] * u[2]),
                    (u[0]*u[6] - (1.0/3.0) * u[1]**2 + (2.0/3.0) * u[2]**2),
                    (( (10/9) * u[3] * u[1] )
                        -  ( (2/3) * (u[4]*u[1] + u[5] * u[2]) ) ),
                    ( ( (10/9) * u[3] * u[2] )
                        -  ( (2/3) * (u[5]*u[1] + u[6] * u[2]) ) )
                    ] 
                )
        nl = 1/(Kn*math.sqrt(ref_theta)) * nl
        nl = np.dot(Symm,nl)
        nl = df.as_vector(nl)
        nl = df.inner(v,nl) * df.dx 
        return nl


    lhs = 0.0 #initialise
    rhs = 0.0 #initialise
    lhs = a + df.lhs(gls)
    if problem_type == "linear":
        rhs = bc + df.rhs(gls)
    elif problem_type == "nonlinear":
        rhs = bc + nonlinearity() + df.rhs(gls)
    else:
        print("Undefined problem_type paramter is passed")
        quit()
    F = lhs - rhs

    #SOLVER
    if problem_type == "linear":
        u_Function = df.Function(V)
        df.solve(lhs==rhs, u_Function,[],solver_parameters={'linear_solver':'mumps'})
        u = u_Function
    elif problem_type == "nonlinear":
        du = df.TrialFunction(V)  #TrialFunctions --> wrong, without 's'
        Jac = df.derivative(F, u, du)
        #user given solver parameters
        abs_tol = input_file["SOLVER_PARAMS"]['newton_abs_tol']
        rel_tol = input_file["SOLVER_PARAMS"]['newton_rel_tol']
        max_itr = input_file["SOLVER_PARAMS"]['newton_max_itr']
        step_size = input_file["SOLVER_PARAMS"]['newton_relaxation_parameter']
        problem = df.NonlinearVariationalProblem(F,u,[],Jac)
        solver = df.NonlinearVariationalSolver(problem)
        solver.parameters ['newton_solver']['linear_solver'] = 'mumps'
        solver.parameters ['newton_solver']['absolute_tolerance'] = abs_tol
        solver.parameters ['newton_solver']['relative_tolerance'] = rel_tol
        solver.parameters ['newton_solver']['maximum_iterations'] = max_itr
        solver.parameters ['newton_solver']['relaxation_parameter'] =step_size
        #df.solve(F == 0, u, [], J=Jac,  solver_parameters={'newton_solver' : {'linear_solver' : 'mumps'}})
        solver.solve()
        #df.solve((a-L)==0, u,[])                                                   

    #===============
    #Post-processing
    #===============
    sol = u.split(deepcopy=True)

    
    def linear_transformation(sol):
        sol[0].vector()[:] = sol[0].vector()[:] * ref_rho
        sol[1].vector()[:] = ref_vx + sol[1].vector()[:] * math.sqrt(ref_theta)
        sol[2].vector()[:] = ref_vy + sol[2].vector()[:] * math.sqrt(ref_theta)
        sol[3].vector()[:] = ref_theta - sol[3].vector()[:] * (2/3) * ref_theta
        sol[4].vector()[:] = sol[4].vector()[:] * ref_theta * ref_rho
        sol[5].vector()[:] = sol[5].vector()[:] * ref_theta * ref_rho
        sol[6].vector()[:] = sol[6].vector()[:] * ref_theta * ref_rho
        sol[7].vector()[:] = -sol[7].vector()[:] * pow(ref_theta,1.5) * ref_rho
        sol[8].vector()[:] = -sol[8].vector()[:] * pow(ref_theta,1.5) * ref_rho
        return sol
    def nonlinear_transformation(sol):
        sol[0].vector()[:] = sol[0].vector()[:] * ref_rho
        sol[1].vector()[:] = ref_vx + \
                np.divide(sol[1].vector()[:] * math.sqrt(ref_theta), sol[0].vector()[:])
        sol[2].vector()[:] = ref_vy + \
                np.divide(sol[2].vector()[:] * math.sqrt(ref_theta),sol[0].vector()[:])
        sol[3].vector()[:] = ref_theta - \
                np.divide(pow(sol[1].vector()[:],2) * ref_theta, 3* pow(sol[0].vector()[:],2)) - \
                np.divide(pow(sol[2].vector()[:],2) * ref_theta, 3* pow(sol[0].vector()[:],2)) - \
                np.divide(2*sol[3].vector()[:]*ref_theta,3*sol[0].vector()[:])
        sol[4].vector()[:] = sol[4].vector()[:] * ref_theta * ref_rho - \
                np.divide(2*pow(sol[1].vector()[:],2) * ref_theta * ref_rho, 3*sol[0].vector()[:]) + \
                np.divide(pow(sol[2].vector()[:],2)*ref_theta * ref_rho, 3*sol[0].vector()[:])
        sol[5].vector()[:] = sol[5].vector()[:] * ref_theta * ref_rho - \
                np.divide(sol[1].vector()[:]*sol[2].vector()[:]*ref_theta*ref_rho, sol[0].vector()[:])
        sol[6].vector()[:] = sol[6].vector()[:] * ref_theta * ref_rho - \
                np.divide(2*pow(sol[2].vector()[:],2)*ref_theta*ref_rho, 3*sol[0].vector()[:]) + \
                np.divide(pow(sol[1].vector()[:],2)*ref_theta *ref_rho, 3*sol[0].vector()[:])
        sol[7].vector()[:] = -sol[7].vector()[:] * pow(ref_theta,1.5) * ref_rho + \
                np.divide(pow(sol[1].vector()[:],3) * pow(ref_theta,1.5)*ref_rho, pow(sol[0].vector()[:],2)) -\
                np.divide(sol[1].vector()[:]*sol[4].vector()[:]*pow(ref_theta,1.5)*ref_rho , sol[0].vector()[:]) - \
                np.divide(sol[2].vector()[:]*sol[5].vector()[:]*pow(ref_theta,1.5)*ref_rho,sol[0].vector()[:] ) + \
                np.divide(sol[1].vector()[:]*pow(sol[2].vector()[:],2)*pow(ref_theta,1.5)*ref_rho, pow(sol[0].vector()[:],2)) +\
                np.divide( 5*sol[1].vector()[:]*sol[3].vector()[:]*pow(ref_theta,1.5)*ref_rho,3*sol[0].vector()[:] )
        sol[8].vector()[:] = -sol[8].vector()[:] * pow(ref_theta,1.5) * ref_rho +\
                np.divide(pow(sol[2].vector()[:],3) * pow(ref_theta,1.5)*ref_rho, pow(sol[0].vector()[:],2)) -\
                np.divide(sol[2].vector()[:]*sol[6].vector()[:]*pow(ref_theta,1.5)*ref_rho , sol[0].vector()[:]) - \
                np.divide(sol[1].vector()[:]*sol[5].vector()[:]*pow(ref_theta,1.5)*ref_rho,sol[0].vector()[:] ) + \
                np.divide(sol[2].vector()[:]*pow(sol[1].vector()[:],2)*pow(ref_theta,1.5)*ref_rho, pow(sol[0].vector()[:],2)) +\
                np.divide( 5*sol[2].vector()[:]*sol[3].vector()[:]*pow(ref_theta,1.5)*ref_rho,3*sol[0].vector()[:] )
        return sol

    sol = linear_transformation(sol)

    def write_func(field_name, variable_name, mesh_num):
        xdmffile_u = df.XDMFFile(df.MPI.comm_world,
                              'results_mathematica/{0}_{1}.xdmf'
                              .format(variable_name,mesh_num))
        xdmffile_u.write(field_name)
        xdmffile_u.close()
    for i in range(input_file["GENERAL_PARAMS"]["number_of_moments"]):
        write_func(sol[i], i, mesh_number)
    print("Program terminated successfully")
    moment_order = input_file["GENERAL_PARAMS"]["moment_order"]
